package org.mekelburger.moritz.scanble;

import org.apache.commons.math3.analysis.MultivariateFunction;

import java.util.List;


/**
 * Created by moritz on 09.11.2014.
 */
public class NelderMead implements MultivariateFunction {
    private List<SimpleBeacon> beacons;

    public NelderMead(List<SimpleBeacon> beacons) {
        this.beacons = beacons;
    }

    public double value(double[] doubles) {
        double result = 0.;
        float[] tmpPos;
        double expectedDistance;
        for (SimpleBeacon b: this.beacons) {
            tmpPos = b.getPosition();
            expectedDistance = Math.pow(Math.pow(tmpPos[0] - doubles[0], 2) +
                                        Math.pow(tmpPos[1] - doubles[1], 2) +
                                        Math.pow(tmpPos[2] - doubles[2], 2), 0.5);
            result += Math.pow(expectedDistance - b.getDistance(), 2);
        }

        return result;
    }
}
