package org.mekelburger.moritz.scanble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.util.Log;


import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optimization.GoalType;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.optimization.direct.NelderMeadSimplex;
import org.apache.commons.math3.optimization.direct.SimplexOptimizer;
// */

/*
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;
// */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by moritz on 08.11.2014.
 */
public class BeaconHandler implements Runnable {
    private final static String TAG = "BeaconHandler";
    private HashMap<String, Beacon> beacons = new HashMap<String, Beacon>();

    private BluetoothAdapter bluetoothAdapter;
    private long lastRequest = 0;
    private final static long INACTIVITY_TIMEOUT = 10000;

    private static final ScheduledExecutorService stopLeScanWorker =
            Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture stopLeScanFuture = null;

    private BluetoothAdapter.LeScanCallback leScanCallback;

    private static HashMap<String, float[]> KNOWN_BEACONS = new HashMap<String, float[]>();
    private boolean scanning;

    public List<SimpleBeacon> getTmpBeacons() {
        return tmpBeacons;
    }

    List<SimpleBeacon> tmpBeacons = new ArrayList<SimpleBeacon>();

    private Runnable stopLe = new Runnable() {
        @Override
        public void run() {
            bluetoothAdapter.stopLeScan(leScanCallback);
            scanning = false;
        }
    };

    public float[] getPos() {
        Log.d(TAG, "Getting position...");
        if (!scanning) {
            bluetoothAdapter.startLeScan(leScanCallback);
            scanning = true;
        }
        synchronized (this.pos) {
            return this.pos;
        }
    }

    private float[] pos = new float[] {0.f, 0.f, 0.f};

    public BeaconHandler(BluetoothManager bluetoothManager) {
        this.bluetoothAdapter = bluetoothManager.getAdapter();
        final BeaconHandler beaconHandler = this;
        this.leScanCallback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                Log.v(TAG, String.format("onLeScan: %s", device.getAddress()));
                beaconHandler.advertisement(device, rssi, scanRecord);
            }
        };
        this.bluetoothAdapter.startLeScan(leScanCallback);
        scanning = true;
        this.stopLeScanFuture = this.stopLeScanWorker.schedule(stopLe, BeaconHandler.INACTIVITY_TIMEOUT, TimeUnit.MILLISECONDS);
        KNOWN_BEACONS.put("00:07:80:68:28:29", new float[]{6.f,-3f,1.f});
        KNOWN_BEACONS.put("00:07:80:C0:FF:EE", new float[]{0.f,-3f,1.f});
        KNOWN_BEACONS.put("00:07:80:68:28:64", new float[]{0.f,3f,2.25f});
//        KNOWN_BEACONS.put("00:07:80:11:11:11", new float[]{0f,0f,0f});
        KNOWN_BEACONS.put("00:07:80:79:00:BA", new float[]{9.f,1.f,0.8f});  // DKBLE BLE113
        KNOWN_BEACONS.put("00:07:80:72:CD:E3", new float[]{6.f,0.f,1.f});  // BLE113 eval-board
        KNOWN_BEACONS.put("00:07:80:7F:41:30", new float[]{-2.f,0.f,2.f});
        KNOWN_BEACONS.put("0E:F3:EE:5A:04:CD", new float[]{0.f,0.f,1.2f});
        KNOWN_BEACONS.put("60:03:08:AE:CB:F2", new float[]{0.f,0.f,0.2f});
    }

    /**
     * TODO: This class is supposed to handle all requests about beacons:
     * <ul>
     *     <li>Add beacons</li>
     *     <li>Add new Advertisements</li>
     *     <li>Tidy up data base
     *     <ul>
     *         <li>Remove old advertisements</li>
     *         <li>Remove ancient beacons</li>
     *     </ul>
     *     </li>
     *     <li>Provide position information on request.</li>
     * </ul>
     * The calculation and clearance actions are executed in a dedicated Runnable. The read and
     * write actions are synchronized.
     *
     * TODO: Implement a server with poll/select functionality to have a more simple setup.
     */
    public void run() {
        Log.d(TAG, "BeaconHandler running...");
        this.lastRequest = System.currentTimeMillis();
        while (true) {
            try {
                Thread.sleep(1000);
                synchronized (this.beacons) {
                    this.tmpBeacons = new ArrayList<SimpleBeacon>();
                    for (String mac : this.beacons.keySet()) {
                        if (this.beacons.get(mac).getDistance() != Float.NaN) {
                            tmpBeacons.add(beacons.get(mac).flatCopy());
                        }
                    }
                }
                double[] tmpPos = new double[]{0.,0.,0.};
                synchronized (this.pos) {
                    for (int i = 0; i < 3; i++) {
                        tmpPos[i] = this.pos[i];
                    }
                }
                if (tmpBeacons.size() >= 4) {
                    // ToDo: Move away from the functions marked as deprecated.
                    // ToDo: Check what all these constants are for and replace them.
/*                    SimplexOptimizer optimizer = new SimplexOptimizer(1e-10, 1e-30);
                    NelderMeadSimplex nms = new NelderMeadSimplex(tmpBeacons.size());
                    nms.iterate(new NelderMead(tmpBeacons), new Comparator<PointValuePair>() {
                        @Override
                        public int compare(PointValuePair lhs, PointValuePair rhs) {
                            return lhs.getValue().compareTo(rhs.getValue());
                        }
                    });

                    PointValuePair optimum = optimizer.optimize(nms, GoalType.MINIMIZE,
                            new InitialGuess(tmpPos));
// */
                    try {
                        SimplexOptimizer optimizer = new SimplexOptimizer(1e-10, 1e-30);
                        optimizer.setSimplex(new NelderMeadSimplex(new double[]{0.15, 0.15, 0.15}));
                        PointValuePair optimum = optimizer.optimize(5000, new NelderMead(tmpBeacons),
                                GoalType.MINIMIZE, tmpPos);
                        Log.d(TAG, String.format("Position: (%s, %s)", optimum.getPoint()[0],
                                optimum.getPoint()[1]));
                        synchronized (this.pos) {
                            for (int i = 0; i < 3; i++) {
                                this.pos[i] = (float) optimum.getPoint()[i];
                            }
                        }
                    } catch (TooManyEvaluationsException e) {

                    }// */
                } else {
                    Log.d(TAG, String.format("Not enough beacons for position recognition: %d",
                            tmpBeacons.size()));
                }
                Log.v(TAG, "BeaconService beaconHandler Thread");
            } catch (InterruptedException e) {
                Log.d(TAG, "BeaconService beaconHanlder sleep interrupted.");
            }

        }
    }

    /**
     * Add a new beacon to the local data base
     * @param beacon Beacon
     */
    private void addBeacon(Beacon beacon){
        synchronized (this.beacons) {
            this.beacons.put(beacon.getMac(), beacon);
        }
    }

    /**
     * Add an rssi value to the data base
     * @param device Bluetooth device that sent the advertisement
     * @param rssi RSSI vcalue
     * @param scanRecord Data of the advertisement
     */
    public void advertisement(BluetoothDevice device, int rssi, byte[] scanRecord) {
        // TODO: Add the data...
        this.lastRequest = System.currentTimeMillis();
        Log.v(TAG, "New advertisement");
        if (this.KNOWN_BEACONS.containsKey(device.getAddress().toUpperCase())) {
            synchronized (this.beacons) {
                Beacon b;
                if (!this.beacons.keySet().contains(device.getAddress())) {
                    b = new Beacon(device, scanRecord, this.KNOWN_BEACONS.get(device.getAddress()));
                    this.addBeacon(b);
                } else {
                    b = this.beacons.get(device.getAddress());
                }
                b.addRssi(rssi);
            }
            Log.v(TAG, String.format("Advertisement from %s", device.getAddress()));
        } else {
            Log.v(TAG, String.format("%s not in KNOWN_BEACONS", device.getAddress()));
        }
    }
}
