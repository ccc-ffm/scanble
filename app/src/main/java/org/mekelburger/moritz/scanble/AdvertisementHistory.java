package org.mekelburger.moritz.scanble;

import android.util.Log;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Set;
import java.util.List;
import java.util.TreeMap;

import mr.go.sgfilter.ContinuousPadder;
import mr.go.sgfilter.SGFilter;

/**
 * This class stores a history of RSSI values of received advertisements with time stamps
 */
public class AdvertisementHistory extends TreeMap<Long, Integer> {
    private final static int OLD = 2000;
    private final static int TIMEOUT = 500;
    private final static String TAG = "AdvertisementHistory";

    public AdvertisementHistory() {
        super();
    }

    /**
     * Add an RSSI value to the data base. It will be stored together with the current time stamp.
     *
     * @param rssi
     */
    public void addAdvertisement(int rssi) {
        this.put(new Long(System.currentTimeMillis()), new Integer(rssi));
    }


    /**
     * Generate a data set where missing data is padded (left padding).
     *
     * As advertisements are sent with a nearly constant rate there should be one data point every
     * n milliseconds. So we get that n as minimum of our data points and pad missing values from
     * the right.
     * All values older then AdvertisementHistory.OLD will be deleted before evaluation, If we got
     * no new results within the last AdvertisementHistory.TIMEOUT milliseconds all data is flushed
     * and an empty data set is returned. If there are not at least two values in the history the
     * result is empty as well.
     *
     * @return padded data set
     */
    private double[] getPaddedData(String beaconID) {
        List<Integer> tmpResult = new ArrayList<Integer>();
        Object[] timestamps;
        long lastTimestamp = 0;
        Long currentTs;

        Log.d(TAG, "getPaddedData started");
        this.getAverageValue();

        if (this.keySet().size() < 2) {
            return new double[]{};
        }

        timestamps = this.keySet().toArray();
        if (System.currentTimeMillis() - ((Long) timestamps[timestamps.length - 1]).longValue() >
                AdvertisementHistory.OLD) {
            Log.d(TAG, String.format("Last time stamp too old: %d - %d = %d > %d",
                    System.currentTimeMillis(),
                    ((Long) timestamps[timestamps.length - 1]).longValue(),
                    System.currentTimeMillis() -
                            ((Long) timestamps[timestamps.length - 1]).longValue(),
                    AdvertisementHistory.OLD));
            return new double[]{};
        }
        Log.v(TAG, String.format("KeySet sorted..: %s", Arrays.toString(timestamps)));

        // get the medium time delta.
        long[] timeDeltas = new long[this.keySet().size() - 1];

        for (int i=0; i < timeDeltas.length; i++) {
            timeDeltas[i] =
                    ((Long) timestamps[i + 1]).longValue() - ((Long) timestamps[i]).longValue();
        }
        long mediumDelta = timeDeltas[timeDeltas.length / 2];

        lastTimestamp = 0;
        for (Object ts: timestamps) {
            currentTs = (Long) ts;
            if (lastTimestamp != 0) {
                while (lastTimestamp == 0 || currentTs.longValue() - lastTimestamp > 1.5 * mediumDelta) {
                    tmpResult.add(new Integer(0));
                    lastTimestamp += mediumDelta;
                }
            }
            lastTimestamp = currentTs.longValue();
            tmpResult.add(this.get(currentTs));
        }
        double[] result = new double[tmpResult.size()];
        for (int i=tmpResult.size() - 1; i >= 0; i--) {
            if (tmpResult.get(i).intValue() == 0) {
                result[i] = result[i+1];
            } else {
                result[i] = (double) tmpResult.get(i);
            }
        }
        Log.v(TAG, String.format("Padded data: %s - %s", beaconID, Arrays.toString(result)));
        return result;
    }

    public float getCurrentValue(String beaconID) {
        double[] dataSet = this.getPaddedData(beaconID);
        if (dataSet.length <= 3) {
            return Float.NaN;
        }
        double[] coeffs = SGFilter.computeSGCoefficients(dataSet.length - 1, 0, 3);
        float result = 0.f;
        for (int i=0; i < dataSet.length; i++) {
//            Log.v(TAG, String.format("Savitzky/Golay %.2f += %.2f * %.2f = %.2f", result, coeffs[i], dataSet[i], coeffs[i] * dataSet[i]));
            result += coeffs[i] * (float) dataSet[i];
        }
        Log.v(TAG, String.format("Savitzky/Golay coefficients: %s", Arrays.toString(coeffs)));
//        Log.d(TAG, String.format("Savitzky/Golay value: %.2f", result));
        return result;
    }

    /**
     * Calculate the mean value of the latest values. Old entries (more then
     * AdvertisementHistory.OLD ms old) are deleted.
     *
     * @return average value of latest RSSI values, Returns NaN if there are no recent RSSI values
     */
    public float getAverageValue() {
        float result = 0.f;
        long now = System.currentTimeMillis();
        Set<Long> recent = new HashSet<Long>();

        for (Long ts: this.keySet()) {
            if (now - ts.longValue() < OLD) {
                recent.add(ts);
                result += this.get(ts).longValue();
            }
        }
        this.keySet().retainAll(recent);
        return result / (float) this.size();
    }

    public float getError() {
        float result = 0.f;
        float m = this.getAverageValue();
        for (Integer v: this.values()) {
            result += Math.pow(v.doubleValue() - m, 2);
        }
        return result/(this.size() - 1);
    }
}
